var squareSize = 50;
var chessBoardSize = 8 * squareSize;
var selectionWidth = 3 * squareSize
var selectionHeight = 8 * squareSize;

var col2index = {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7};
var cols = "abcdefgh";

var globalState = {"squares": [], "pieces": []}; // Global state used to display board
var selectionPieces = [];

var selectedPiece = undefined; // Have we clicked to select a piece to move it? If yes, this stores the piece object. Otherwise it is undefined

function setupBoard () {
    var canvas = document.getElementById("chessboard-canvas");
    canvas.width = chessBoardSize;
    canvas.height = chessBoardSize;
    var ctx = canvas.getContext("2d");

    for (var j = 0; j < 8; j++) {
        for (var i = 0; i < 8; i++) {
            var x = i * squareSize;
            var y = j * squareSize;
            var square = {"left": x, "top": y, "height": squareSize, "width": squareSize,
                          "col": cols[i], "row": 8 - j};
            if ((i + j) % 2 === 0) {
                square.color = colors["white"];
            } else {
                square.color = colors["black"];
            }
            globalState.squares.push(square);
        }
    }

    globalState.squares.forEach(function(square) {drawSquare(ctx, square)});

    initPieces(ctx);

    addClickListeners();
}


function drawAllSquares() {
    var ctx = document.getElementById("chessboard-canvas").getContext("2d");
    globalState.squares.forEach(square => drawSquare(ctx, square));
}

function drawAllPieces() {
    var ctx = document.getElementById("chessboard-canvas").getContext("2d");
    globalState.pieces.forEach(piece => drawPiece(ctx, piece));
}



function setupSelectionCanvas() {
    var canvas = document.getElementById("pieceselection-canvas");
    canvas.width = selectionWidth;
    canvas.height = selectionHeight;
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = "#FFFFFF";
    ctx.strokeRect(0, 0, selectionWidth, selectionHeight);



    initSelectionPieces(ctx);

    
    //addSelectionListener(selectionPieces);
}


function initSelectionPieces(ctx) {
    var piece2Pos = {"BlackRook": [0, 0],
                     "BlackKnight": [squareSize, 0],
                     "BlackBishop": [2 * squareSize, 0],
                     "BlackQueen": [0, squareSize],
                     "BlackKing": [squareSize, squareSize],
                     "BlackPawn": [2 * squareSize, squareSize],
                     "WhiteRook": [0, 6 * squareSize],
                     "WhiteKnight": [squareSize, 6 * squareSize],
                     "WhiteBishop": [2 * squareSize, 6 * squareSize],
                     "WhiteQueen": [0, 7 * squareSize],
                     "WhiteKing": [squareSize, 7 * squareSize],
                     "WhitePawn": [2 * squareSize, 7 * squareSize]}


    for (var piece in piece2Pos) {
        var img = new Image();
        img.src = "pieces/" + piece + ".png";

        // This is a weird hack but it is the only way I could get it to work
        if (piece === "WhitePawn") {
            img.onload = function () {selectionPieces.forEach(function(piece) {drawSelectionPiece(ctx, piece);})};
        }
        var color, type;
        if (piece.startsWith("White")) {
            color = "white";
            type = piece.slice(5).toLowerCase();
        } else {
            color = "black";
            type = piece.slice(5).toLowerCase();
        }
        
        var pieceObj = {"type": type, "color": color, "img": img,
                        "x": piece2Pos[piece][0], "y": piece2Pos[piece][1]};
        selectionPieces.push(pieceObj);
    }
}




function initPieces(ctx) {
    // TODO This should really be an array.
    // Would clean up code below a little bit
    var piece2Pos = {"WhiteRook1": ["a", 1],
                     "WhiteKnight1": ["b", 1],
                     "WhiteBishop1": ["c", 1],
                     "WhiteQueen": ["d", 1],
                     "WhiteKing": ["e", 1],
                     "WhiteBishop2": ["f", 1],
                     "WhiteKnight2": ["g", 1],
                     "WhiteRook2": ["h", 1]};

    for (var i = 1; i <= 8; i++) {
        var pawnName = "WhitePawn" + i;
        piece2Pos[pawnName] = ["abcdefgh"[i-1], 2];
    }

    var piece2Pos2 = {"BlackRook8": ["a", 8],
                     "BlackKnight8": ["b", 8],
                     "BlackBishop8": ["c", 8],
                     "BlackQueen": ["d", 8],
                     "BlackKing": ["e", 8],
                     "BlackBishop2": ["f", 8],
                     "BlackKnight2": ["g", 8],
                     "BlackRook2": ["h", 8]};

    for (var i = 1; i <= 8; i++) {
        var pawnName = "BlackPawn" + i;
        piece2Pos2[pawnName] = ["abcdefgh"[i-1], 7];
    }

    piece2Pos = {...piece2Pos, ...piece2Pos2};
    

    
    for (var [index, [piece, pos]] of Object.entries(Object.entries(piece2Pos))) {
        var img = new Image();
        var fname = piece;
        var lastChar = fname.charAt(fname.length - 1); //fname.slice(-1)
        if ("12345678".includes(lastChar)) {
            fname = fname.slice(0, fname.length - 1);
        }
        img.src = "pieces/" + fname + ".png";
        var [col, row] = piece2Pos[piece];
        var res = coord2xy(col, row);

        // This is a weird hack but it is the only way I could get it to work
        if (piece === "BlackPawn8") {
            img.onload = function () {globalState.pieces.forEach(function(piece) {drawPiece(ctx, piece);})};
        }
        var color, type;
        if (piece.startsWith("White")) {
            color = "white";
            type = piece.slice(5).toLowerCase();
        } else {
            color = "black";
            type = piece.slice(5).toLowerCase();
        }
        if ("123456789".includes(type.charAt(type.length - 1))) {
            type = type.slice(0, type.length-1);
        }
        
        var pieceObj = {"type": type, "color": color, "col": col, "row": row, "img": img,
                        "img": img, "id": index};
        
        globalState.pieces.push(pieceObj);
    }
}

function findAvailableId() {
    var newid = 0;
    var ids = globalState.pieces.map(function(piece) {return piece.id});

    while (newid in ids) {
        newid += 1;
    }
    return newid;
}


function movePiece(ctx, selectedPiece, col, row) {
    // Redraw square on old position
    var oldcol = selectedPiece.col;
    var oldrow = selectedPiece.row;

    // TODO Movement logic here
    selectedPiece.col = col;
    selectedPiece.row = row;

    

    
    if (oldcol !== undefined) {
        var oldPosSquare = globalState.squares.find(function (sq) { return sq.col === oldcol && sq.row === oldrow });
        drawSquare(ctx, oldPosSquare);
    } else {
        // It is a piece from the selection board
        // Needs to have col and row set here for data to be valid
        var newId = findAvailableId();
        selectedPiece.id = newId;
        selectedPiece.type = selectedPiece.type;
        globalState.pieces.push(selectedPiece);
    }

    // If there are any pieces in the new position they need to be removed
    var existingPiece = globalState.pieces.find(piece => piece.col === col && piece.row === row
                                               && piece.id !== selectedPiece.id);
    if (existingPiece !== undefined) {
        globalState.pieces = globalState.pieces.filter(piece => piece.id !== existingPiece.id);
    }
    
    // Redraw piece in new position
    drawPiece(ctx, selectedPiece);
}

function resetBoard() {
    globalState = {"squares": [], "pieces": []};
    
    var canvas = document.getElementById("chessboard-canvas");
    canvas.width = chessBoardSize;
    canvas.height = chessBoardSize;
    var ctx = canvas.getContext("2d");

    for (var j = 0; j < 8; j++) {
        for (var i = 0; i < 8; i++) {
            var x = i * squareSize;
            var y = j * squareSize;
            var square = {"left": x, "top": y, "height": squareSize, "width": squareSize,
                          "col": cols[i], "row": 8 - j};
            if ((i + j) % 2 === 0) {
                square.color = colors["white"];
            } else {
                square.color = colors["black"];
            }
            globalState.squares.push(square);
        }
    }

    globalState.squares.forEach(function(square) {drawSquare(ctx, square)});

    initPieces(ctx);
}

function clearBoard() {
    globalState.pieces = [];
    var ctx = document.getElementById("chessboard-canvas").getContext("2d");
    for (var j = 0; j < 8; j++) {
        for (var i = 0; i < 8; i++) {
            var index = j * 8 + i;
            if ((i + j) % 2 === 0) {
                globalState.squares[index].color = colors["white"];
            } else {
                globalState.squares[index].color = colors["black"];
            }
        }
    }
    globalState.squares.forEach(square => drawSquare(ctx, square));
}


function handleMainCanvasClick(event, divLeft, divTop) {
    var canvas = document.getElementById("chessboard-canvas");
    var ctx = canvas.getContext("2d");
    var x = event.pageX - divLeft;
    var y = event.pageY - divTop;
    
    var col = cols[Math.floor(x / squareSize)];
    var row = parseInt(8 - Math.floor(y / squareSize));
    
    var pieceOnSquare = globalState.pieces.find(function (piece) { return piece.row === row && piece.col === col; });
    if (selectedPiece !== undefined) {
        movePiece(ctx, selectedPiece, col, row);
        selectedPiece = undefined;
    } else {
        selectedPiece = pieceOnSquare;
    }
}


function handleSelectionBoardClick(event, divLeft, divTop) {
    if (selectedPiece !== undefined) {
        var canvas = document.getElementById("chessboard-canvas");
        var ctx = canvas.getContext("2d");
        // Remove piece
        globalState.pieces = globalState.pieces.filter(piece => piece.id !== selectedPiece.id);
        // Redraw origin square unless the selectedPiece is from selectionBoard
        if (selectedPiece.id !== undefined) {
            drawSquareRC(ctx, selectedPiece.row, selectedPiece.col);
        }
        // Unset selectedPiece
        selectedPiece = undefined;
        return;
    }

    
    var x = event.pageX - divLeft - chessBoardSize;
    var y = event.pageY - divTop;


    var col = parseInt(Math.floor(x / squareSize)) * squareSize;
    var row = parseInt(Math.floor(y / squareSize)) * squareSize;
    

    var pieceClicked = selectionPieces.find(function (piece)
                                            { return piece.x === col && piece.y === row; });
    var newPiece = {};
    selectedPiece = Object.assign(newPiece, pieceClicked);
}


function swapColor(event, divLeft, divTop) {
    // Check which square was clicked and change its color
    event.preventDefault();
    var canvas = document.getElementById("chessboard-canvas");
    var ctx = canvas.getContext("2d");
    var x = event.pageX - divLeft;
    var y = event.pageY - divTop;
    
    for (var i = 0; i < globalState.squares.length; i++) {
        var square = globalState.squares[i];
        if (y > square.top && y < square.top + square.height
            && x > square.left && x < square.left + square.width) {
            if (square.color === colors["white"]) {
                square.color = colors["black"];
            } else {
                square.color = colors["white"];
            }
            
            drawSquare(ctx, square);
            
            var pieceOnSquare = globalState.pieces.find(function (piece) { return piece.row === square.row && piece.col === square.col; });
            if (pieceOnSquare !== undefined) {
                drawPiece(ctx, pieceOnSquare);
            }        
        }
    }
}



function addClickListeners () {
    var divcont = document.getElementById("canvas-container");
    divLeft = divcont.offsetLeft + divcont.clientLeft;
    divTop = divcont.offsetTop + divcont.clientTop;

    function selectOrMovePiece (event) {
        if (event.target.id === "chessboard-canvas") {
            handleMainCanvasClick(event, divLeft, divTop);
        } else {
            handleSelectionBoardClick(event, divLeft, divTop);
        }
    }


    function swapColorOfClicked (event) {
        if (event.target.id !== "chessboard-canvas") {
            return;
        } else {
            swapColor(event, divLeft, divTop);
        }
    }

    divcont.addEventListener("click", selectOrMovePiece);
    divcont.addEventListener("contextmenu", swapColorOfClicked);
}


var colors = {"white": "#DDDDDD", "black": "#AAAAAA"};

function drawSquare(ctx, square) {
    ctx.fillStyle = square.color;
    ctx.fillRect(square.left, square.top, square.width, square.height);
    ctx.strokeRect(square.left, square.top, square.width, square.height);
}


function drawSquareRC(ctx, row, col) {
    var square = globalState.squares.find(square => square.row === row && square.col === col);
    drawSquare(ctx, square);
}



function coord2xy(col, row) {
    var x = col2index[col] * squareSize;
    var y = (8 - row) * squareSize;
    return {"x": x, "y": y};
}


function drawPiece(ctx, piece) {
    var res = coord2xy(piece.col, piece.row);
    ctx.drawImage(piece.img, res.x, res.y, squareSize, squareSize);
}


function drawSelectionPiece(ctx, piece) {
    ctx.drawImage(piece.img, piece.x, piece.y, squareSize, squareSize);
}



function countAttackers() {
    var attackCounts = new Array(64).fill(0); // Squares are indices in row-major order
    
    // For each square, determine how many pieces are attacking it
    for (var index in globalState.pieces) {
        var piece = globalState.pieces[index];
        var moves = [];
        if (piece.type === "knight") {
            moves = getKnightMoves(piece.col, piece.row);
        }
        if (piece.type === "bishop") {
            moves = getBishopMoves(piece.col, piece.row);
        }

        if (piece.type === "rook") {
            moves = getRookMoves(piece.col, piece.row);
        }

        if (piece.type === "queen") {
            moves = getQueenMoves(piece.col, piece.row);
        }

        if (piece.type === "king") {
            moves = getKingMoves(piece.col, piece.row);
        }

        if (moves.length === 0) {
            continue;
        }
        for (var move of moves) {
            var squareIndex = col2index[move[0]] + (8 - move[1]) * 8;
            attackCounts[squareIndex] += 1; 
        }
    }
    return attackCounts
}

function addHexColor(c1, c2) {
    c1 = c1.replace("#", "");
    c2 = c2.replace("#", "");
    var hexStr = (parseInt(c1, 16) + parseInt(c2, 16)).toString(16);
    while (hexStr.length < 6) { hexStr = '0' + hexStr; } // Zero pad.
    return "#" + hexStr;
}

function subtractHexColor(c1, c2) {
    c1 = c1.replace("#", "");
    c2 = c2.replace("#", "");
    var hexStr = (parseInt(c1, 16) - parseInt(c2, 16)).toString(16);
    while (hexStr.length < 6) { hexStr = '0' + hexStr; } // Zero pad.
    return "#" + hexStr;
}

function red(c) {
    return c.slice(1, 3);
}


function colorAttackedSquares() {
    var attackCounts = countAttackers();
    for (var i = 0; i < 64; i++) {
        var count = attackCounts[i];

        // We require that the squares are row-major ordered
        var color = globalState.squares[i].color;
        if (count !== 0) {
            //color = addHexColor(color, "#0" + (10 * count).toString() + "0000");
            color = getAttackColor(count);
        } else {
            var ix = 8 - globalState.squares[i].row;
            var jx = col2index[globalState.squares[i].col];
            if ((ix + jx) % 2 == 0) {
                color = colors["white"];
            } else {
                color = colors["black"];
            }
        }
        globalState.squares[i].color = color;
    }
    drawAllSquares();
    drawAllPieces();
}


function getAttackColor(attackerCount) {
    // var baseColor = "#DDDDFF";
    // var basehexstr = attackerCount.toString(16);
    // var hexstr = basehexstr;
    // while (hexstr.length < 6) {hexstr = hexstr + basehexstr};
    // var color = subtractHexColor(baseColor, hexstr);

    var color;
    var count2color = [];
    //["#ddDDff", "#ccCCdd", "#bbBBcc",
    //"#aaAAbb", "#9999aa", "#888899",
    // "#777788", "#666677"];
    for (var i = 0; i < 16; i++) {
        var base = parseInt("ddDDff", 16);
        var subtracter = parseInt("111111", 16) * i * 2;
        if (base - subtracter < 0) {
            count2color.push("#000000");
        } else {
            count2color.push("#" + (base - subtracter).toString(16));
        }
    }
    if (attackerCount - 1 >= count2color.length) {
        color = "#000000";
    } else {
        color = count2color[attackerCount - 1];
    }

    return color;

}


function getKnightMoves(col, row) {
    var colindex = col2index[col];
    var rowindex = 8 - row;


    // TODO Do it a better way
    var candidates = [[colindex - 1, rowindex - 2],
                      [colindex - 1, rowindex + 2],
                      [colindex + 1, rowindex - 2],
                      [colindex + 1, rowindex + 2],
                      [colindex - 2, rowindex - 1],
                      [colindex - 2, rowindex + 1],
                      [colindex + 2, rowindex - 1],
                      [colindex + 2, rowindex + 1]];

    
    candidates = candidates.filter(candy => candy[0] >= 0 && candy[0] <= 7
                                   && candy[1] >= 0 && candy[1] <= 7);

    var moves = candidates.map(candy => [cols[candy[0]], 8 - candy[1]]);
    return moves;
}


function getBishopMoves(col, row) {
    var colindex = col2index[col];
    var rowindex = 8 - row;

    var candidates = [];

    for (var i = -8; i <= 8; i++) {
        if (i === 0) {continue;}
        candidates.push([colindex + i, rowindex + i]);
        candidates.push([colindex + i, rowindex - i]);
    }
    // TODO refactor so these are functions. Probably will be used in all
    // getMoves
    candidates = candidates.filter(candy => candy[0] >= 0 && candy[0] <= 7
                                   && candy[1] >= 0 && candy[1] <= 7);
    candidates = candidates.sort(candy => candy[0]);
    var moves = candidates.map(candy => [cols[candy[0]], 8 - candy[1]]);
    return moves;
}


function getRookMoves(col, row) {
    var colindex = col2index[col];
    var rowindex = 8 - row;

    var candidates = [];

    for (var i = -8; i <= 8; i++) {
        if (i === 0) {continue;}
        candidates.push([colindex + i, rowindex]);
        candidates.push([colindex, rowindex + i]);
    }
    // TODO refactor so these are functions. Probably will be used in all
    // getMoves
    candidates = candidates.filter(candy => candy[0] >= 0 && candy[0] <= 7
                                   && candy[1] >= 0 && candy[1] <= 7);
    candidates = candidates.sort(candy => candy[0]);
    var moves = candidates.map(candy => [cols[candy[0]], 8 - candy[1]]);
    return moves;
}


function getQueenMoves(col, row) {
    var rmoves = getRookMoves(col, row);
    var bmoves = getBishopMoves(col, row);

    return rmoves.concat(bmoves);
}

function getKingMoves(col, row) {
    var colindex = col2index[col];
    var rowindex = 8 - row;


    var candidates = [[colindex - 1, rowindex - 1],
                      [colindex - 1, rowindex],
                      [colindex - 1, rowindex + 1],
                      [colindex, rowindex - 1],
                      [colindex, rowindex + 1],
                      [colindex + 1, rowindex - 1],
                      [colindex + 1, rowindex],
                      [colindex + 1, rowindex + 1]];
    candidates = candidates.filter(candy => candy[0] >= 0 && candy[0] <= 7
                                   && candy[1] >= 0 && candy[1] <= 7);
    candidates = candidates.sort(candy => candy[0]);
    var moves = candidates.map(candy => [cols[candy[0]], 8 - candy[1]]);
    return moves;
}





setupBoard();
setupSelectionCanvas();
